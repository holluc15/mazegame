package com.holzmann.lukas;

import com.badlogic.gdx.Gdx;

/**
 * Created by Lukas on 02.01.2018.
 */

public class GameConstants {
    public static float screenWidth = Gdx.graphics.getWidth();
    public static float screenHeight = Gdx.graphics.getHeight();
    public static int V_WIDTH = 400;
    public static int V_HEIGHT = 208;


    public static boolean drawTouchPad = false;
    public static boolean drawLines = false;

}
