package com.holzmann.lukas;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;


/**
 * Created by Admin on 19.06.2017.
 */

public class HUD implements Disposable {
    public static Stage stage;

    public int worldTimer;
    private float timeCount;

    private final Label countdownLabel;
    private  Label  fpsLabel;
    private final BitmapFont font;

    public Label timeupLabel;
    public Label clickTextLabel;


    public HUD(SpriteBatch sb) {
        worldTimer = 300;
        timeCount = 0;
        Viewport viewport = new FitViewport(GameConstants.screenWidth,GameConstants.screenHeight, new OrthographicCamera());
        stage = new Stage(viewport,sb);


        font = new BitmapFont();

        Table table = new Table();
        table.top(); // allign in the top of our stage
        table.setFillParent(true); // resize the table to our screen size

        timeupLabel = new Label("TIME IS UP!",new Label.LabelStyle(font, Color.WHITE));
        timeupLabel.setSize(GameConstants.screenWidth/4,GameConstants.screenHeight/4);
        timeupLabel.setFontScale(2f);
        clickTextLabel = new Label("click to play again!",new Label.LabelStyle(font, Color.WHITE));
        clickTextLabel.setSize(GameConstants.screenWidth/4,GameConstants.screenHeight/4);
        clickTextLabel.setFontScale(1.25f);
        countdownLabel = new Label(String.format("%03d",worldTimer),new Label.LabelStyle(font, Color.WHITE));
        fpsLabel = new Label(String.format("%03d", Gdx.graphics.getFramesPerSecond()),new Label.LabelStyle(font, Color.WHITE));
        Label timeLabel = new Label("TIME", new Label.LabelStyle(font, Color.WHITE));
        Label levelLabel = new Label("1-1", new Label.LabelStyle(font, Color.WHITE));
        Label gameLabel = new Label("FPS", new Label.LabelStyle(font, Color.WHITE));
        Label worldLabel = new Label("WORLD", new Label.LabelStyle(font, Color.WHITE));

        table.add(gameLabel).expandX().padTop(10);
        table.add(worldLabel).expandX().padTop(10);
        table.add(timeLabel).expandX().padTop(10);
        table.row();
        table.add(fpsLabel).expandX();
        table.add(levelLabel).expandX();
        table.add(countdownLabel).expandX();
        Table table2 = new Table();
        table2.center(); // allign in the top of our stage
        table2.setFillParent(true); // resize the table to our screen size
        table2.row();
        table2.add(timeupLabel);
        table2.row();
        table2.row();
        table2.add(clickTextLabel);
        timeupLabel.setVisible(false);
        clickTextLabel.setVisible(false);


        stage.addActor(table);
        stage.addActor(table2);

    }

    public void update(float delta) {
        timeCount += delta;
        if(timeCount >= 1) {
            worldTimer--;
            fpsLabel.setText(String.format("%03d", Gdx.graphics.getFramesPerSecond()));
            countdownLabel.setText(String.format("%03d",worldTimer));
            timeCount = 0;
        }
    }

    @Override
    public void dispose() {
        stage.dispose();
        font.dispose();
    }
}

