package com.holzmann.lukas;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Lukas on 06.01.2018.
 */

public class CameraMovement {

    private OrthographicCamera camera;
    public boolean finish = false;
    private float time = 0;


    float timeToCameraZoomTarget, cameraZoomTarget, cameraZoomOrigin, cameraZoomDuration;
    public CameraMovement(PlayScreen screen, Vector2 mapSize) {
        camera = screen.getCamera();

        camera.position.x = mapSize.x/2/MazeGame.PPM;
        camera.position.y = mapSize.y/2/MazeGame.PPM;
        camera.zoom = 2.25f;


       zoomTo(0.2f,7.5f);
    }

    public void update(float delta) {
        time += delta;
        if(time >= 3.5f) {


            if (timeToCameraZoomTarget >= 0) {
                timeToCameraZoomTarget -= delta;
                float progress = timeToCameraZoomTarget < 0 ? 1 : 1f - timeToCameraZoomTarget / cameraZoomDuration;
                camera.zoom = Interpolation.swingIn.apply(cameraZoomOrigin, cameraZoomTarget, progress);

            }else{
                finish = true;

            }

        }
    }

    public void zoomTo (float newZoom, float duration){
        cameraZoomOrigin = camera.zoom;
        cameraZoomTarget = newZoom;
        timeToCameraZoomTarget = cameraZoomDuration = duration;
    }

    public boolean finished() {
        return finish;
    }


}
