package com.holzmann.lukas;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.DragListener;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;


public class PlayScreen implements Screen {

    public SpriteBatch batch;
    private MazeGame game;
    private OrthographicCamera camera;
    private Viewport viewport;
    private final Box2DDebugRenderer b2dr;
    private World world;
    private Player player;
    private HUD hud;
    private TiledMap map;
    private OrthogonalTiledMapRenderer renderer;
    private B2WorldCreator creator;
    private CameraMovement cameraMovement;

    public PlayScreen(MazeGame game) {

        this.game = game;
        this.batch = game.batch;
        camera = new OrthographicCamera();

        viewport = new StretchViewport(GameConstants.screenWidth / MazeGame.PPM,GameConstants.screenHeight/ MazeGame.PPM,camera);
        hud = new HUD(game.batch);
        b2dr = new Box2DDebugRenderer();

        world = new World(new Vector2(0, 0), true); // 1 Parameter Gravity

        //controller = new Controller(this);

        TmxMapLoader maploader = new TmxMapLoader();
        map = maploader.load("maze.tmx");
        renderer = new OrthogonalTiledMapRenderer(map, 1 / MazeGame.PPM);





        creator = new B2WorldCreator(this);
        player = new Player(this, getTiledMapSize());
        cameraMovement = new CameraMovement(this,getTiledMapSize());

    }

    public Vector2 getTiledMapSize() {
        MapProperties prop = map.getProperties();

        int mapWidth = prop.get("width", Integer.class);
        int mapHeight = prop.get("height", Integer.class);
        int tilePixelWidth = prop.get("tilewidth", Integer.class);
        int tilePixelHeight = prop.get("tileheight", Integer.class);

        int mapPixelWidth = mapWidth * tilePixelWidth;
        int mapPixelHeight = mapHeight * tilePixelHeight;
        return new Vector2(mapPixelWidth,mapPixelHeight);
    }



    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {

            update(delta);

            Gdx.gl.glClearColor(255 / 255f, 87 / 255f, 51 / 255f, 1);
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);


            renderer.render();


            if (GameConstants.drawLines) {
                b2dr.render(world, camera.combined);
            }

            game.batch.setProjectionMatrix(camera.combined);

            game.batch.begin();

            player.draw(game.batch);


            game.batch.end();
            game.batch.setProjectionMatrix(HUD.stage.getCamera().combined);
            HUD.stage.draw();


    }

    public void update(float delta) {
        if(cameraMovement.finished() && hud.worldTimer >= 0) {
            handleInput();
        }else if(!cameraMovement.finished()){
            cameraMovement.update(delta);
        }else{
          hud.timeupLabel.setVisible(true);
          hud.clickTextLabel.setVisible(true);
        }


        world.step(1 / 60f, 6, 2);



            player.update(delta);
            //camera follows Player
        float lerp = 0.1f;
        Vector3 position = this.getCamera().position;
        position.x += (player.b2body.getPosition().x - position.x) * lerp;
        position.y += (player.b2body.getPosition().y - position.y) * lerp;
        camera.position.set(position);



        hud.update(delta);


        camera.update();
        renderer.setView(camera);

    }

    public void handleInput() {
        boolean available = Gdx.input.isPeripheralAvailable(Input.Peripheral.Accelerometer);

        if(available)
        player.b2body.applyLinearImpulse(new Vector2(Gdx.input.getAccelerometerY() / MazeGame.PPM, (Gdx.input.getAccelerometerX() / MazeGame.PPM)*-1), player.b2body.getWorldCenter(), true);


    }



    //Getter
    public TiledMap getMap() {
        return map;
    }
    public World getWorld() {
        return world;
    }
    public OrthographicCamera getCamera() {
        return camera;
    }
    public Viewport getViewport() {
        return viewport;
    }
    public Player getPlayer() {
        return player;
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        world.dispose();
        b2dr.dispose();
        hud.dispose();

    }
}
