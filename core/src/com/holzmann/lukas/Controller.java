package com.holzmann.lukas;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import javax.print.DocFlavor;

public class Controller implements ApplicationListener {

    private OrthographicCamera camera;
    private Stage stage;
    private SpriteBatch batch;
    private Touchpad touchpad;
    private Touchpad.TouchpadStyle touchpadStyle;
    private Skin touchpadSkin;
    private Drawable touchBackground;
    private Drawable touchKnob;
    private Player player;
    private float speed = 0.25f;
    public String direction = "";

    public Controller(PlayScreen screen) {
        camera = screen.getCamera();
        batch = screen.batch;
        player = screen.getPlayer();

        //Create a touchpad skin
        touchpadSkin = new Skin();
        //Set background image
        touchpadSkin.add("touchBackground", new Texture("touchBackground.png"));
        //Set knob image
        touchpadSkin.add("touchKnob", new Texture("touchKnob.png"));
        //Create TouchPad Style
        touchpadStyle = new Touchpad.TouchpadStyle();
        //Create Drawable's from TouchPad skin
        touchBackground = touchpadSkin.getDrawable("touchBackground");
        touchKnob = touchpadSkin.getDrawable("touchKnob");
        //Apply the Drawables to the TouchPad Style
        if(GameConstants.drawTouchPad) {
            touchpadStyle.background = touchBackground;
            touchpadStyle.knob = touchKnob;
        }

        //Create new TouchPad with the created style
        touchpad = new Touchpad(10, touchpadStyle);


        //Create a Stage and add TouchPad
        stage = HUD.stage;
        stage.addActor(touchpad);
        Gdx.input.setInputProcessor(stage);
        touchpad.setBounds(0,0 ,GameConstants.screenWidth, GameConstants.screenHeight);


    }

    public void setPosition(int x,int y) {

        //touchpad.setPosition(x - touchpad.getWidth()/2,y-touchpad.getHeight()/2);
    }

    @Override
    public void create() {

    }

    @Override
    public void dispose() {

    }

    public String getDirection(float x, float y) {
        return "";
    }

    @Override
    public void render() {
        //Move blockSprite with TouchPad





        if(player.b2body.getLinearVelocity().x < 2 && player.b2body.getLinearVelocity().x > -2 && player.b2body.getLinearVelocity().y < 9.81f) {
            player.b2body.applyLinearImpulse(new Vector2(touchpad.getKnobPercentX() * speed, 0), player.b2body.getWorldCenter(), true);
        }





        stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();

    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void resize(int width, int height) {

    }
}