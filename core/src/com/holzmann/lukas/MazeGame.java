package com.holzmann.lukas;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class MazeGame extends Game {
	 public SpriteBatch batch;

	public static final short GROUND_BIT = 1;
	public static final short PLAYER_BIT = 2;

	public static float PPM = 100;
	
	@Override
	public void create () {
		batch = new SpriteBatch();

		setScreen(new PlayScreen(this));
	}

	
	@Override
	public void dispose () {
		batch.dispose();
	}
}
