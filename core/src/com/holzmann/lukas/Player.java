package com.holzmann.lukas;


import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.viewport.Viewport;

/**
 * Created by Lukas on 03.01.2018.
 */

public class Player extends Sprite {

    private final World world;
    public Body b2body;
    private Viewport viewport;
    private Sprite playerTexture = new Sprite(new Texture("player.png"));
    private Vector2 mapSize;


    public Player(PlayScreen screen, Vector2 mapSize) {
        super(new Sprite(new Texture("player.png")));

        this.world = screen.getWorld();
        this.mapSize = mapSize;
        viewport = screen.getViewport();

        definePlayer();
        System.out.println(mapSize.x + "  " + mapSize.y);
        setBounds(mapSize.x/2,mapSize.y/2,32 / MazeGame.PPM,32 / MazeGame.PPM);
        setRegion(playerTexture);



    }

    private void definePlayer() {
        BodyDef bdef = new BodyDef();
        bdef.position.set(mapSize.x/2/ MazeGame.PPM,mapSize.x/2/ MazeGame.PPM);
        bdef.type = BodyDef.BodyType.DynamicBody;

        b2body = world.createBody(bdef);

        FixtureDef fdef = new FixtureDef();
        fdef.restitution = 0.315f;
        CircleShape shape = new CircleShape();
        shape.setRadius(16 / MazeGame.PPM);

        fdef.shape = shape;
        b2body.createFixture(fdef);


    }

    public void update(float deltaTime) {
        setPosition(b2body.getPosition().x -getWidth()/ 2,b2body.getPosition().y - getHeight()/ 2);
        setRegion(playerTexture);
        System.out.println(b2body.getPosition().x + "  " + b2body.getPosition().y);

    }



}
